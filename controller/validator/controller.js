const express = require('express');
const SECRET = process.env.SECRET || "validate";
const app = express();

let test = require('./challengeTester.js').test;

app.post(`/${SECRET}`, function(req, res, next) {
	res.json({
		solved: false,
		message: "/SECRET",
	});
});

app.get(`/${SECRET}/test`, function(req, res, next) {
	test().then(response=>{
		res.json({
			solved: response,
			message: "/SECRET/test",
		});
	});
	});


const PORT = process.env.CONTROLLER_PORT || 5555;
app.listen(PORT, () => {
	console.log(`controller listening on ${PORT} with secret: ${SECRET}`);
});

app.get(`/${SECRET}/app/users.html`, function(req, res, next) {
	res.sendFile("/home/user/so lvable/app/users.html");
});