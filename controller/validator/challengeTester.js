/**
 * Created by Zsolt on 3/24/2018.
 */
let rp = require('request-promise');
const url = "http://localhost:8888/profile";
let path = require('path');

let optionsForNormalLogin = {
	method: 'POST',
	url: url,
	form: {
		username: "Agilitis",
		password: "agilitis"
	}
};

let optionsForBadLogin = {
	method: 'POST',
	url: url,
	form: {
		username: "asdfasdf",
		password: "asdfasdfasdf"
	}
};

let optionsForMalformedLogin = {
	method: 'POST',
	url: url,
	form: {
		username: {'': 'a'},
		password: "asdfasdfasdf"
	}
};
let optionsForInjection ={
	method: 'POST',
	url: url,
	form: {
		username: {'$gt': ''},
		password: {'$gt': ''}
	}
};
let test = ()=>{
	return rp(optionsForBadLogin).then(res=> {
		return rp(optionsForNormalLogin);
	}).then(()=>{
		return rp(optionsForMalformedLogin);
	}).then(()=>{
		return rp(optionsForInjection);
	}).then(()=>{
		return false;
	}).catch(err=>{
		console.log(err);
		return (err.statusCode === 500 || err.statusCode === 403);
	});
};

module.exports = {
	test: test
};



