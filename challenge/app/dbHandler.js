/**
 * Created by Zsolt on 3/24/2018.
 */
'use strict';
const MongoClient = require('mongodb').MongoClient;
let db;
let collectionName;
module.exports = {
    initConnection: (connectionString, coll) => {
        return MongoClient.connect(connectionString)
            .then(dbInstance => {
                db = dbInstance.db('users');
                collectionName = coll;
		        //console.log(db.collection(collectionName));
            })
    },
    insertDocument: (obj) => {
        return db.collection(collectionName)
            .insertOne(obj);
    },
    findOne: (query) => {
        return db.collection(collectionName)
            .findOne(query)
    }
};