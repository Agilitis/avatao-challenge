'use strict';

/**
 * Created by Zsolt on 3/24/2018.
 */

let port = process.env.PORT || 1337;
let express = require('express');
let bodyParser = require('body-parser');
let path = require('path');

const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const dbHandler = require('./dbHandler');
const app = express();
// *** SOLUTION #1
const dollarDefender = require('dollar-defender-middleware');

app.set('port', process.env.PORT || 5000);
//This is the part that causes the problem!
app.use(bodyParser.urlencoded());
// **** SOLUTION #1
app.use(dollarDefender());
// Connection URL
const url = 'mongodb://localhost:27017/avatao';
// Database Name



dbHandler.initConnection(url, 'users').then(() => {
		app.listen(app.get('port'), function () {
			console.log('Node app is running on port', app.get('port'));
		});
		console.log("Connected to documentdb");
		}).catch(err => {
			console.log(err)
		});

app.get('/login', (req, res) => {
	res.sendFile(path.resolve('./index.html'));
});


app.post('/profile', (req, res) => {

	//Here we query the database with a username and password
	dbHandler.findOne({username: req.body.username, password: req.body.password}).then((databaseResponse)=>{
		//Upon recieving back a response - if that is not null -  the user is authenticated because we found him in the database
		console.log(req.body);
		console.log(databaseResponse);
		if(databaseResponse){
			res.send(databaseResponse);
			}else{
			res.send("Unauthorized!");
		}
	}).catch(err=>{
		console.log(err);
	});

});

