NodeJS-Mongo Injection
## Where is the vulnerability?

Cost: 10%

A widely known exploit is the SQL injection. Basically everyone heard about it. Now, one could assume that in a NoSQL database there wasn't a possibility for injection however with the right HTML request, the authentication can be bypassed!

## The HTTP request

Cost: 20%

*username\[\$gt\]=&password\[$gt\]=*
This request will authenticate us, as this: 
```javascript
db.inventory.find( { qty: { $gt: 20 } } )
```
is a valid request to MongoDB. It chooses in the 'inventory' collection 
 those documents whose 'qty' field is *greater than* 20. 

Now, the above http request tells that the user name and password are: 
 ```javascript
 {$gt: ""}
 ```
 This should got you thinking: what documents do satisfy the requirement to be greater than an empty string? Well.... basically every document! This is terrible, our request will respond will *something*. We probably don't know what the respond would be but it is enough that it will contain data. 
## Which NodeJS package is at fault?

Cost: 60%

The body-parser  module will parse the HTTP request as a javascript object which is bad for us, as this request will pass the NoSQL database this: `{"username":{"$gt":""},"password":{"$gt":""}}`. This basically fits all documents in our database and will give us back something, so the user will not be null, as a result we will authorize the intruder without him giving us anything at all!
```javascript
   db.collection('users').findOne({
        username: req.body.username,
        password: req.body.password
    }, (err, user) => {
        if (err) { ... }
        if (user) {
            // authorized
        } else {
            // not authorized
        }
    });
```

## Complete solution

For this problem you can find a big variety of solutions. You can for example use a package which you can easily find using www.npms.io. The one we are using is called 'dollar-defender-middleware', which you can just pass to your epxress app. 
You will have to install the package first using npm:
```javascript
npm install dollar-defender-middleware
```
Then you can simply require it, and give it to your previously set up express app, like the example below. You will have to add these new lines to your code.
```javascript
const dollarDefender = require('dollar-defender-middleware');

app.use(dollarDefender());
```
From now on, if your app recieves a request described above, it will respond with a HTML 500 saying: "Error, dollar injector attack detected!"

Lessons learned

 - MongoDB can be attacked by injection.
 - NPMS.io is a good friend of yours. It gives you crucial information about packages.
 - Using node packages can be very convenient, but one must be careful! They are not nearly flawless. They cannot replace circumspect development.



