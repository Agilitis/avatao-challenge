﻿#### Description
In this challenge, you will see an example of NodeJS + MongoDB injection. MongoDB is a NoSQL database. You will get a faulty code that let's an intruder to bypass authentication with the proper request to the server.
You will see a login window for "authentication". Of course this is not a real life application. Let's suppose that this is a login surface for the admins to check the database's data. If you are authenticated you should see data with usernames and matching passwords.
You can try it by using the 'admin' - > 'password' combination to log in. 
#### Your job
You will have to expand the code so that it will prevent the attackers from using the mentioned request and the site only lets 'real' users to log in. It is very important that after you fix the problem, your server should respond with an *error* if someone tries to bypass your authentication.

